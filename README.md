## Project Name & Pitch

Facilities Booking Website

A website to market the futsal facilities that The ARK is offering to the public. The website allows customer to search for the available slots, book and make payment for the slots that they want.

## Project Screen Shot(s)

![Screenshot](static/screenshot.png?raw=true "Title")

## Installation and Setup Instructions

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```
