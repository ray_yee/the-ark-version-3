require("dotenv").config();
process.env.DEBUG = "nuxt:*";

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "universal",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  dev: process.env.NODE_ENV !== "production",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */ head: {
    titleTemplate: title =>
      "Futsal Pitches in Singapore | Football/Street Soccer Court Booking",
    meta: [
      {
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content:
          "The Ark futsal pitches in Funan, Orchid Country Club, Cuppage and Plaza 8 Singapore are great spots to play your favorite football game. Visit now for booking street soccer court!"
      }
    ]
  },
  static: {
    prefix: false
  },
  webfontloader: {
    google: {
      families: ["Montserrat:100,300,400,500,600,900&display=swap"]
    }
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    {
      src: "~/plugins/cloudinary"
    },
    {
      src: "~/plugins/googlemaps"
    }
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/vuetify"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/moment",
    // Doc: https://github.com/nuxt/content
    "@nuxtjs/recaptcha",
    "@nuxtjs/device",
    "nuxt-webfontloader",
    "@nuxtjs/pwa",
    "nuxt-stripe-module",
    [
      "@nuxtjs/firebase",
      {
        config: {
          production: {
            apiKey: "AIzaSyBVBL1-xQzjr0CrjNxzvQ9XnZ7bO3N-C7Y",
            authDomain: "thearksg-191fb.firebaseapp.com",
            databaseURL: "https://thearksg-191fb.firebaseio.com",
            projectId: "thearksg-191fb",
            storageBucket: "thearksg-191fb.appspot.com",
            messagingSenderId: "762891286483"
          },
          development: {
            // apiKey: "AIzaSyAbb0JBwkRnlZAgbN5VQQCq3X3HQ-VJaI8",
            // authDomain: "thearksg-dev.firebaseapp.com",
            // databaseURL: "https://thearksg-dev.firebaseio.com",
            // projectId: "thearksg-dev",
            // storageBucket: "thearksg-dev.appspot.com",
            // messagingSenderId: "591398632797",
            // appId: "1:591398632797:web:f8f118dc59ede3d62fbcb4",
            // measurementId: "G-HTF2RFVDKD"
            apiKey: "AIzaSyBVBL1-xQzjr0CrjNxzvQ9XnZ7bO3N-C7Y",
            authDomain: "thearksg-191fb.firebaseapp.com",
            databaseURL: "https://thearksg-191fb.firebaseio.com",
            projectId: "thearksg-191fb",
            storageBucket: "thearksg-191fb.appspot.com",
            messagingSenderId: "762891286483"
          }
        },
        services: {
          auth: {
            persistence: "local",
            initialize: {
              onAuthStateChangedMutation: null,
              onAuthStateChangedAction: "ON_AUTH_STATE_CHANGED_ACTION"
            },
            ssr: {
              // !! NEVER deploy a service account file to github or to a publicly accessible folder on your server !!
              credential: "~/assets/firebase/serviceAccountKey.json",
              // Experimental Feature, use with caution.
              serverLogin: {
                sessionLifetime: 60 * 60 * 1000, // one hour
                loginDelay: 50 // minimal recommended delay
              }
            }
          },
          storage: true,
          realtimeDb: true
        }
      }
    ]
  ],
  recaptcha: {
    /* reCAPTCHA options */
    siteKey: "6Lf4gJkUAAAAAF7-VYM51w4hDsxOjz5tAwTSxJQa", // Site key for requests
    version: 2
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */

  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  moment: {
    defaultTimezone: "Asia/Singapore"
  },
  stripe: {
    version: "v3",
    // publishableKey:
    //   "pk_test_51GszXyC7Sr16L1mV1xLWD4KJr1LxVAMKEMvYsLlcYwOOUW8CY8JC997ZmIDLD3ltSmVipzB3Oc6Lbni9Q5kWsEyx00qgT6WlkD"
    publishableKey: "pk_live_CQDJ7u3bMfArSM7N7WKnDe3l00lklxm1yW"
  },
  vuetify: {
    treeShake: true,
    customVariables: ["~/assets/variables.scss"],
    theme: {
      themes: {
        light: {
          primary: "#000",
          secondary: "#b0bec5",
          accent: "#0A8A44",
          error: "#b71c1c"
        }
      }
    },
    icons: {
      iconfont: "mdi" || "mdiSvg" // || 'md' || 'fa' || 'fa4' || 'faSvg'
    }
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    analyze: false,
    filenames: {
      app: ({ isDev }) => (isDev ? "[name].js" : "[contenthash:7].js")
    },
    extend(config, { isClient }) {
      // Extend only webpack config for client-bundle
      if (isClient) {
        config.devtool = "cheap-module-source-map";
      }
    }
  }
};
