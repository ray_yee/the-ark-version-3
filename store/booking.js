export const state = () => ({
  booking: null,
  package: null
});

export const mutations = {
  setBooking(state, payload) {
    state.booking = payload;
  },
  setPackage(state, payload) {
    state.package = payload;
  }
};
