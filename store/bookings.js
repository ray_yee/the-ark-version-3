import moment from "moment";

export const state = () => ({
  bookings: [],
  bookedSlots: {},
  todayBookedSlots: {},
  myBookings: []
});

export const mutations = {
  setBookings(state, payload) {
    let l = [];
    Object.keys(payload).forEach(key => {
      l.push({
        ...payload[key],
        key
      });
    });
    state.bookings = l;
  },
  setBookedSlots(state, payload) {
    state.bookedSlots = payload;
  },
  setTodayBookedSlots(state, payload) {
    state.todayBookedSlots = payload;
  },
  setMyBookings(state, payload) {
    state.myBookings = payload;
  }
};

export const actions = {
  GET_BOOKINGS({ commit }) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("/bookings").on("value", snapshot => {
        commit("setBookings", snapshot.val());
        resolve();
      });
    });
  },
  GET_MY_BOOKINGS({ commit, rootState }) {
    return new Promise((resolve, reject) => {
      if (rootState.user.uid) {
        this.$fireDb
          .ref("/bookings")
          .orderByChild("email")
          .equalTo(rootState.user.email)
          .on("value", snapshot => {
            if (snapshot.val()) {
              const val = snapshot.val();
              let parsedBookings = [];
              let promisesA = [];
              Object.keys(val).forEach(key => {
                const { slots, paymentStatus } = val[key];
                if (paymentStatus !== "Pending") {
                  let a = new Promise((rs, rj) => {
                    let promisesB = slots.map(slot => {
                      return new Promise((res, rej) => {
                        this.$fireDb
                          .ref(`/bookedSlots/${slot}`)
                          .once("value", snapshot => {
                            res(snapshot.val());
                          });
                      });
                    });
                    Promise.all(promisesB).then(resp => {
                      parsedBookings.push({
                        ...val[key],
                        key,
                        slotsExpanded: resp
                      });
                      rs();
                    });
                  });
                  promisesA.push(a);
                }
              });
              Promise.all(promisesA).then(resp => {
                commit("setMyBookings", parsedBookings);
                resolve();
              });
            } else {
              resolve();
            }
          });
      }
      resolve();
    });
  },

  GET_BOOKEDSLOTS({ commit }, { location, date }) {
    return new Promise((resolve, reject) => {
      this.$fireDb
        .ref("/bookedSlots")
        .orderByChild("location")
        .equalTo(location)
        .once("value", snapshot => {
          let val = snapshot.val();
          let parsedVal = {};
          if (val !== null) {
            Object.keys(val).map(key => {
              const { paymentMethod, paymentStatus, submittedDate, date } = val[
                key
              ];
              let expiryDate = moment(submittedDate).add(10, "minutes");
              if (
                paymentMethod === "PayNow" &&
                paymentStatus === "Pending" &&
                moment().isSameOrAfter(expiryDate)
              ) {
              } else {
                let nextday = moment(date).add(1, "day");
                if (moment(date).isSame(date, "day")) {
                  parsedVal[key] = {
                    ...val[key],
                    key
                  };
                } else if (moment(date).isSame(nextday, "day")) {
                  parsedVal[key] = {
                    ...val[key],
                    key
                  };
                }
              }
            });
            commit("setBookedSlots", parsedVal);
          }
          if (Object.keys(parsedVal).length === 0) {
            parsedVal = null;
          }
          resolve(parsedVal);
        });
    });
  },
  ADD_BOOKING({ commit, dispatch }, { bookingData, selectedTimeslots }) {
    return new Promise((resolve, reject) => {
      const {
        location,
        name,
        contact,
        email,
        paymentMethod,
        paymentStatus
      } = bookingData;
      let promises = [];
      let newBookingKey = this.$fireDb
        .ref()
        .child("bookings")
        .push().key;
      Object.keys(selectedTimeslots).map(key => {
        let slot = selectedTimeslots[key];
        slot.map(async k => {
          let slotData = {
            location,
            name,
            contact,
            email,
            submittedDate: moment().format(),
            bookingKey: newBookingKey,
            paymentMethod,
            paymentStatus: paymentStatus ? paymentStatus : null,
            ...k,
            date: moment(k.date, "YYYY-MM-DD").format()
          };
          let a = dispatch("ADD_BOOKEDSLOT", slotData);
          promises.push(a);
        });
      });
      Promise.all(promises).then(res => {
        let updates = {};
        updates["/bookings/" + newBookingKey] = {
          ...bookingData,
          slots: res.map(r => r.slotKey)
        };
        this.$fireDb.ref().update(updates);
        resolve(res);
      });
    });
  },
  ADD_BOOKEDSLOT({ commit }, data) {
    return new Promise((resolve, reject) => {
      let newSlotKey = this.$fireDb
        .ref()
        .child("bookedSlots")
        .push().key;
      let updates = {};
      updates["/bookedSlots/" + newSlotKey] = data;
      this.$fireDb.ref().update(updates);
      resolve({ slotKey: newSlotKey, data });
    });
  },
  UPDATE_BOOKING({ commit }, data) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("bookings/" + data.key).update(data);
      resolve();
    });
  },
  DELETE_BOOKING({ commit }, key) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("bookings/" + key).remove();
      resolve();
    });
  }
};
