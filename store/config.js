export const state = () => ({
  config: {}
});

export const mutations = {
  setConfig(state, payload) {
    state.config = payload
  }
};

export const actions = {
  GET_CONFIG({ commit }) {
    return new Promise((resolve, reject) => {
        this.$fireDb.ref("/config").on("value", snapshot => {
        commit("setConfig", snapshot.val());
      });
      resolve();
    });
  },
  
  UPDATE_CONFIG({ commit }, data) {
    return new Promise((resolve, reject) => {
        this.$fireDb.ref("config").update(data);
      resolve();
    });
  }
};
