import moment from "moment";
import orderBy from "lodash/orderBy";

export const state = () => ({
  userPackages: [],
  creditsLeft: null,
  actualCreditsLeft: null
});

export const mutations = {
  setUserPackages(state, payload) {
    let l = [];
    let creditsLeft = 0;
    let actualCreditsLeft = 0;
    Object.keys(payload).forEach(key => {
      if (
        payload[key].creditsLeft &&
        payload[key].creditsLeft > 0 &&
        moment().isSameOrBefore(payload[key].expiryDate, "day")
      ) {
        l.push({
          ...payload[key],
          key
        });
        if (payload[key].paymentMethod !== "Refund") {
          actualCreditsLeft += payload[key].creditsLeft;
        }
        creditsLeft += payload[key].creditsLeft;
      } else if (
        payload[key].creditsLeft === undefined &&
        moment().isSameOrBefore(payload[key].expiryDate, "day")
      ) {
        l.push({
          ...payload[key],
          key
        });
        if (payload[key].paymentMethod !== "Refund") {
          actualCreditsLeft += payload[key].creditsLeft;
        }
        creditsLeft += parseInt(payload[key].value);
      }
    });
    state.userPackages = orderBy(
      l,
      p => moment(p.expiryDate).format("YYYYMMDD"),
      "asc"
    );
    state.creditsLeft = creditsLeft;
    state.actualCreditsLeft = actualCreditsLeft;
  },
  resetUserPackages(state) {
    state.userPackages = [];
  }
};

export const actions = {
  ADD_CREDIT_PACKAGE({ commit, dispatch }, { creditPackageData }) {
    return new Promise((resolve, reject) => {
      let newCreditPackageKey = this.$fireDb
        .ref("creditPackages")
        .push(creditPackageData).key;
      resolve(newCreditPackageKey);
    });
  },
  UPDATE_CREDIT_PACKAGE({ commit, dispatch }, data) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("creditPackages/" + data.key).update(data);
      resolve();
    });
  },
  GET_USER_CREDIT({ commit }, userKey) {
    return new Promise((resolve, reject) => {
      this.$fireDb
        .ref("creditPackages")
        .orderByChild("userKey")
        .equalTo(userKey)
        .on("value", snapshot => {
          if (snapshot.val() !== null) {
            const val = snapshot.val();
            let paidCredits = {};
            Object.keys(val).forEach(key => {
              if (val[key].paymentStatus !== "Pending") {
                paidCredits[key] = { ...val[key] };
              }
            });
            commit("setUserPackages", paidCredits);
            resolve();
          } else {
            resolve();
          }
        });
    });
  }
};
