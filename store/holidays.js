export const state = () => ({
  holidays: []
});

export const mutations = {
  setHolidays(state, payload) {
    state.holidays = payload;
  }
};

export const actions = {
  GET_HOLIDAYS({ commit }) {
    return new Promise((resolve, reject) => {
      const year = this.$moment().year();
      const nextYear = this.$moment()
        .add(1, "year")
        .year();
      let holidays = [];
      this.$fireDb.ref("/publicHolidays").once("value", snapshot => {
        const val = snapshot.val();
        let holidays = [];
        if (val[year]) {
          holidays = [...val[year]];
        }
        if (val[nextYear]) {
          holidays = [...holidays, ...val[nextYear]];
        }
        commit("setHolidays", holidays);
        resolve();
      });
    });
  }
};
