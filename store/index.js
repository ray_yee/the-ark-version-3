import moment from "moment";

export const strict = false;

export const state = () => ({
  user: null,
  loading: false,
  signup: false
});

export const mutations = {
  setSignUp(state, payload) {
    state.signup = payload;
  },
  setUser(state, payload) {
    state.user = payload;
  },
  setLoading(state, payload) {
    state.loading = payload;
  }
};

export const actions = {
  ON_AUTH_STATE_CHANGED_ACTION({ commit, state }, { authUser, claims }) {
    return new Promise(async (resolve, reject) => {
      if (authUser && state.signup === false) {
        await this.$fireDb
          .ref("users/" + authUser.uid)
          .once("value", async snapshot => {
            if (snapshot.val() === null) {
              this.$fireAuth.signOut();
              commit("setUser", null);
              resolve(false);
            } else {
              commit("setUser", {
                emailVerified: authUser.emailVerified,
                ...authUser.providerData[0],
                name:
                  authUser.providerData[0].providerId === "password"
                    ? authUser.providerData[0].name
                    : authUser.providerData[0].displayName,
                ...snapshot.val(),
                uid: authUser.uid
              });
              if (authUser.uid) {
                await this.$fireDb
                  .ref("creditPackages")
                  .orderByChild("userKey")
                  .equalTo(authUser.uid)
                  .on("value", snapshot => {
                    if (snapshot.val() !== null) {
                      const val = snapshot.val();
                      let paidCredits = {};
                      Object.keys(val).forEach(key => {
                        if (val[key].paymentStatus !== "Pending") {
                          paidCredits[key] = { ...val[key] };
                        }
                      });
                      commit("credits/setUserPackages", paidCredits);
                      resolve();
                    } else {
                      resolve();
                    }
                  });
              }
            }
          });
      } else {
        resolve();
      }
    });
  },
  IS_LOADING({ commit }, { message, status }) {
    commit("setLoading", status);
  },
  SIGN_IN_WITH_EMAIL({ commit }, { email, password }) {
    return new Promise((resolve, reject) => {
      this.$fireAuth
        .signInWithEmailAndPassword(email, password)
        .then(res => {
          this.$fireDb.ref("users/" + res.user.uid).once("value", snapshot => {
            if (snapshot.val() !== null) {
              commit("setUser", {
                emailVerified: res.user.emailVerified,
                ...res.user.providerData[0],
                ...snapshot.val()
              });
            }
            resolve(true);
          });
          //commit('setUser', buildUserObject(res))
        })
        .catch(err => {
          resolve(false);
        });
    });
  },
  GOOGLE_SIGNUP({ commit }) {
    const GoogleAuthProvider = new this.$fireAuthObj.GoogleAuthProvider();
    GoogleAuthProvider.addScope(
      "https://www.googleapis.com/auth/userinfo.email"
    );
    return new Promise((resolve, reject) => {
      commit("setSignUp", true);
      this.$fireAuth
        .signInWithPopup(GoogleAuthProvider)
        .then(res => {
          if (res.additionalUserInfo.isNewUser) {
            const {
              uid,
              displayName,
              email,
              phoneNumber,
              emailVerified,
              providerData
            } = res.user;
            const userdata = {
              name: displayName,
              email,
              emailVerified,
              contact: phoneNumber,
              photoURL: providerData[0].photoURL,
              providerId: providerData[0].providerId,
              joined: moment().format(),
              uid
            };
            this.$fireDb.ref("users/" + uid).set(userdata);
            resolve({ userdata, error: false });
          } else {
            resolve({ error: true });
          }
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          resolve({ error });
          // ...
        });
    });
  },
  FACEBOOK_SIGNUP({ commit }) {
    const FacebookAuthProvider = new this.$fireAuthObj.FacebookAuthProvider();
    return new Promise((resolve, reject) => {
      commit("setSignUp", true);

      FacebookAuthProvider.setCustomParameters({
        display: "popup"
      });
      this.$fireAuth
        .signInWithPopup(FacebookAuthProvider)
        .then(res => {
          if (res.additionalUserInfo.isNewUser) {
            const {
              uid,
              displayName,
              email,
              phoneNumber,
              emailVerified,
              providerData
            } = res.user;
            const userdata = {
              name: displayName,
              email,
              emailVerified,
              contact: phoneNumber,
              photoURL: providerData[0].photoURL,
              providerId: providerData[0].providerId,
              joined: moment().format(),
              uid
            };
            this.$fireDb.ref("users/" + uid).set(userdata);
            resolve({ userdata, error: false });
          } else {
            resolve({ error: true });
          }
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          resolve({ error });
          // ...
        });
    });
  },

  SIGN_IN_GOOGLE({ commit }) {
    const GoogleAuthProvider = new this.$fireAuthObj.GoogleAuthProvider();
    return new Promise((resolve, reject) => {
      this.$fireAuth
        .signInWithPopup(GoogleAuthProvider)
        .then(res => {
          this.$fireDb.ref("users/" + res.user.uid).once("value", snapshot => {
            if (snapshot.val() !== null) {
              commit("setUser", {
                emailVerified: res.user.emailVerified,
                ...res.user.providerData[0],
                ...snapshot.val()
              });
              resolve(true);
            } else {
              this.$fireAuth.signOut();
              commit("setUser", null);
              resolve(false);
            }
          });
        })
        .catch(err => {
          resolve(false);
        });
    });
  },
  SIGN_IN_FACEBOOK({ commit }) {
    const FacebookAuthProvider = new this.$fireAuthObj.FacebookAuthProvider();
    return new Promise((resolve, reject) => {
      FacebookAuthProvider.setCustomParameters({
        display: "popup"
      });
      this.$fireAuth
        .signInWithPopup(FacebookAuthProvider)
        .then(res => {
          this.$fireDb.ref("users/" + res.user.uid).once("value", snapshot => {
            if (snapshot.val() !== null) {
              commit("setUser", {
                emailVerified: res.user.emailVerified,
                ...res.user.providerData[0],
                ...snapshot.val()
              });
              resolve(true);
            } else {
              this.$fireAuth.signOut();
              commit("setUser", null);
              resolve(false);
            }
          });
        })
        .catch(err => {
          console.log(err);
          resolve(false);
        });
    });
  },

  SIGN_OUT({ commit }) {
    this.$fireAuth
      .signOut()
      .then(() => {
        commit("setUser", null);
        commit("credits/resetUserPackages");
      })
      .catch(err => console.log(err));
  },

  RESET_PASSWORD({ commit }, email) {
    return new Promise((resolve, reject) => {
      this.$fireAuth
        .sendPasswordResetEmail(email)
        .then(() => {
          resolve();
        })
        .catch(function(error) {
          console.log(error);
          resolve({ error });
          // An error happened.
        });
    });
  },
  ADD_SSO_USER({ commit }, { data, uid }) {
    return new Promise(async (resolve, reject) => {
      await this.$fireDb.ref("users/" + uid).set(data);
      commit("setUser", {
        ...data
      });
      resolve(true);
    });
  },

  ADD_USER({ commit }, data) {
    return new Promise((resolve, reject) => {
      const { email, password, contact, name } = data;
      this.$fireAuth
        .createUserWithEmailAndPassword(email, password)
        .then(async res => {
          const { emailVerified, providerData, uid } = res.user;
          const user = this.$fireAuth.currentUser;
          user.sendEmailVerification();
          await user.updateProfile({
            displayName: name
          });
          const userdata = {
            name,
            email,
            emailVerified,
            contact,
            providerId: providerData[0].providerId,
            joined: moment().format(),
            uid
          };
          this.$fireDb.ref("users/" + uid).set(userdata);
          commit("setUser", userdata);
          resolve(userdata);
        })
        .catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          reject(error);
          // ...
        });
    });
  },
  UPDATE_USER({ commit }, data) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("users/" + data.key).update(data);
      resolve();
    });
  }
};
