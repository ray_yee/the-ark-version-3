export const state = () => ({
  locations: []
});

export const mutations = {
  setLocations(state, payload) {
    let l = [];
    Object.keys(payload).forEach(key => {
      if (payload[key].active) {
        l.push({
          ...payload[key],
          key
        });
      }
    });
    state.locations = l;
  }
};

export const actions = {
  GET_LOCATIONS({ commit }) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("/locations").on("value", snapshot => {
        const data = snapshot.val();
        const locationList = Object.keys(data).map(key => data[key].name);
        if (data) {
          commit("setLocations", snapshot.val());
        }
        resolve(locationList);
      });
    });
  }
};
