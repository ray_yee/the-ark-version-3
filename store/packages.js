export const state = () => ({
  packages: []
});

export const mutations = {
  setPackages(state, payload) {
    let p = [];
    Object.keys(payload).forEach(key => {
      p.push({
        ...payload[key],
        key
      });
    });
    state.packages = p;
  }
};

export const actions = {
  GET_PACKAGES({ commit }) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("/packages").on("value", snapshot => {
        if (snapshot.val() !== null) {
          commit("setPackages", snapshot.val());
          resolve();
        } else {
          resolve();
        }
      });
    });
  }
};
