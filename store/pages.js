export const state = () => ({
  page: null,
  news: null
});

export const mutations = {
  setPage(state, payload) {
    state.page = payload;
  },
  setNews(state, payload) {
    state.news = payload;
  }
};

export const actions = {
  GET_NEWS({ commit }) {
    return new Promise((resolve, reject) => {
      this.$fireDb
        .ref("/pages")
        .orderByChild("type")
        .equalTo("News")
        .on("value", snapshot => {
          if (snapshot.val() !== null) commit("setNews", snapshot.val());
          resolve(snapshot.val());
        });
    });
  },
  GET_PAGE({ commit }, name) {
    return new Promise((resolve, reject) => {
      this.$fireDb
        .ref("/pages")
        .orderByChild("name")
        .equalTo(name)
        .on("value", snapshot => {
          if (snapshot.val() !== null) commit("setPage", snapshot.val());
          resolve(snapshot.val());
        });
    });
  },
  GET_PAGE_BY_URL({ commit }, url) {
    return new Promise((resolve, reject) => {
      this.$fireDb
        .ref("/pages")
        .orderByChild("url")
        .equalTo(url)
        .on("value", snapshot => {
          if (snapshot.val() !== null) commit("setPage", snapshot.val());
          resolve(snapshot.val());
        });
    });
  }
};
