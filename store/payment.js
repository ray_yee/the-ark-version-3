export const mutations = {
  updatePayment(state, payload) {
    state.paid = payload;
  }
};

export const state = () => ({
  paid: null
});

export const actions = {
  UPDATE_PAYMENT_STATUS({ commit }, bookingKey) {
    this.$fireDb
      .ref("bookings/" + bookingKey + "/paymentStatus")
      .on("value", snapshot => {
        if (snapshot.val() && snapshot.val() === "Paid") {
          commit("updatePayment", snapshot.val());
        }
      });
  },
  UPDATE_CREDIT_STATUS({ commit }, creditPackageKey) {
    this.$fireDb
      .ref("creditPackages/" + creditPackageKey + "/paymentStatus")
      .on("value", snapshot => {
        if (snapshot.val() && snapshot.val() === "Paid") {
          commit("updatePayment", snapshot.val());
        }
      });
  }
};
