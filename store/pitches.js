export const state = () => ({
  pitches: []
});

export const mutations = {
  setPitches(state, payload) {
    let p = [];
    Object.keys(payload).forEach(key => {
      p.push({
        ...payload[key],
        key
      });
    });
    state.pitches = p;
  }
};

export const actions = {
  GET_PITCHES({ commit }) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("/pitches").on("value", snapshot => {
        if (snapshot.val() !== null) commit("setPitches", snapshot.val());
        resolve();
      });
    });
  }
};
