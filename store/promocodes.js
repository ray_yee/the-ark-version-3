export const state = () => ({
  promocodes: null
});

export const mutations = {
  setPromocodes(state, payload) {
    let p = [];
    Object.keys(payload).forEach(key => {
      p.push({
        ...payload[key],
        key
      });
    });
    state.promocodes = p;
  }
};

export const actions = {
  GET_PROMOCODES({ commit }, name) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("/promocode").on("value", snapshot => {
        if (snapshot.val() !== null) commit("setPromocodes", snapshot.val());
        resolve(snapshot.val());
      });
    });
  }
};
