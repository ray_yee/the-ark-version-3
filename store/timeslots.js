export const state = () => ({
  timeslots: []
});

export const mutations = {
  setTimeslots(state, payload) {
    let p = [];
    Object.keys(payload).forEach(key => {
      p.push({
        ...payload[key],
        key
      });
    });
    state.timeslots = p;
  },
  updateTimeslots(state, payload) {
    return state.timeslots.map(timeslot);
  }
};

export const actions = {
  GET_TIMESLOTS({ commit }) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("timeslots").on("value", snapshot => {
        if (snapshot.val() !== null) commit("setTimeslots", snapshot.val());
      });
      resolve();
    });
  },
  ADD_TIMESLOT({ commit }, data) {
    return new Promise((resolve, reject) => {
      let newTimeslotKey = this.$fireDb.ref("timeslots").push().key;
      let updates = {};
      updates["timeslots/" + newTimeslotKey] = data;
      this.$fireDb.ref().update(updates);
      resolve();
    });
  },
  UPDATE_TIMESLOT({ commit }, data) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("timeslots/" + data.key).update(data);
      commit("updateTimeslot", data);
      resolve();
    });
  },
  DELETE_TIMESLOT({ commit }, key) {
    return new Promise((resolve, reject) => {
      this.$fireDb.ref("timeslots/" + key).remove();
      resolve();
    });
  }
};
